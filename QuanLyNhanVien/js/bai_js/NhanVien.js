function layThongTinTuFrom() {
    var _tknv = document.getElementById("tknv").value;
    var _ten = document.getElementById("name").value;
    var _email = document.getElementById("email").value;
    var _pass = document.getElementById("password").value;
    var _ngaylam = document.getElementById("datepicker").value;
    var _luongCB = document.getElementById("luongCB").value * 1;
    var _chucvu = document.getElementById("chucvu").value;
    var _giolam = document.getElementById("gioLam").value * 1;

    return new Nhanvien(
        _tknv,
        _ten,
        _email,
        _pass,
        _ngaylam,
        _luongCB,
        _chucvu,
        _giolam
    );
}

function renderDSNV(nhanVienArr) {
    var contentHTML = "";
    for (var index = 0; index < nhanVienArr.length; index++) {
        var nhanVien = nhanVienArr[index];
        var contentTr = `<tr>
                         <th>${nhanVien.tknv}</th>
                         <th>${nhanVien.ten}</th>
                         <th>${nhanVien.email}</th>
                         <th>${nhanVien.ngayLam}</th>
                         <th>${nhanVien.chucVu}</th>
                         <th>${nhanVien.tongLuong()}</th>
                         <th>${nhanVien.xepLoai()}</th>
                         <th>
                         <button onclick="xoaNV('${nhanVien.tknv
            }')" class="btn btn-danger mb-2">Xoá</button>
                         <button onclick="suaNV('${nhanVien.tknv
            }')" data-toggle="modal"
                         data-target="#myModal" class="btn btn-warning mb-2">Sửa</button>
                         </th>
                         </tr>`;
        contentHTML += contentTr;
    }
    return (document.getElementById("tableDanhSach").innerHTML = contentHTML);
}

function timKiemViTri(id, arr) {
    var ViTri = -1;
    for (var index = 0; index < arr.length; index++) {
        if (arr[index].tknv == id) {
            ViTri = index;
        }
    }
    return ViTri;
}

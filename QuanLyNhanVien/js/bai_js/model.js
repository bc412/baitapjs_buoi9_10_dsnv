function Nhanvien(
    _tknv,
    _ten,
    _email,
    _pass,
    _ngaylam,
    _luongCB,
    _chucvu,
    _giolam
) {
    this.tknv = _tknv;
    this.ten = _ten;
    this.email = _email;
    this.pass = _pass;
    this.ngayLam = _ngaylam;
    this.luongCB = _luongCB;
    this.chucVu = _chucvu;
    this.gioLam = _giolam;

    this.tongLuong = function () {
        if (this.chucVu == "Sếp") {
            return (this.luongCB * 3).toLocaleString();
        } else if (this.chucVu == "Trưởng phòng") {
            return (this.luongCB * 2).toLocaleString();
        } else if (this.chucVu == "Nhân viên") {
            return this.luongCB.toLocaleString();
        }
    };

    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return (document.getElementById(
                "tbGiolam"
            ).innerHTML = `Nhân viên xuất sắc`);
        } else if (this.gioLam >= 176) {
            return (document.getElementById("tbGiolam").innerHTML = `Nhân viên giỏi`);
        } else if (this.gioLam >= 160) {
            return (document.getElementById("tbGiolam").innerHTML = `Nhân viên khá`);
        } else if (this.gioLam < 160) {
            return (document.getElementById(
                "tbGiolam"
            ).innerHTML = `Nhân viên trung bình`);
        }
    };
}


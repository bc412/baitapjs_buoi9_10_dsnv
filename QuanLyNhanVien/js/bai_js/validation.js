function kiemTraKhongDuocDeTrong(value, idErr) {
    if (value == "" || value == "Chọn chức vụ") {
        return (
            (document.getElementById(idErr).innerHTML = `Không được để trống`),
            (document.getElementById(idErr).style.display = "block"),
            false
        );
    } else {
        return (document.getElementById(idErr).innerHTML = ``), true;
    }
}

function kiemTraSo(value) {
    console.log(value);
    var re = /^\d+$/;
    var isNumber = re.test(value);
    if (isNumber) {
        return (document.getElementById("tbTKNV").innerHTML = ``), true;
    } else {
        return (
            (document.getElementById("tbTKNV").innerHTML = `Mã sinh viên chỉ có số`),
            (document.getElementById("tbTKNV").style.display = "block"),
            false
        );
    }
}

function kiemTraChu(value) {
    var re =
        /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    var isNumber = re.test(value);
    if (isNumber) {
        return (document.getElementById("tbTen").innerHTML = ``), true;
    } else {
        return (
            (document.getElementById(
                "tbTen"
            ).innerHTML = `Tên nhân viên phải là chữ`),
            (document.getElementById("tbTen").style.display = "block"),
            false
        );
    }
}

function kiemTraTrung(idNV, nhanVienArr) {
    // console.log(idNV, nhanVienArr);
    // /findIndex return vị trí của item nếu điều kiện true, nếu không tìm thấy trả về -1
    var ViTri = nhanVienArr.findIndex(function (item) {
        return item.tknv == idNV;
    });
    // console.log(ViTri);
    if (ViTri != -1) {
        return (
            (document.getElementById("tbTKNV").innerHTML =
                "Tài khoản nhân viên đã tồn tại"),
            (document.getElementById("tbTKNV").style.display = "block"),
            false
        );
    } else {
        return (document.getElementById("tbTKNV").innerHTML = ""), true;
    }
}

function kiemTraDoDai(value, idErr, min, max) {
    var length = value.length;
    console.log(length);
    if (length < min || length > max) {
        return (
            (document.getElementById(
                idErr
            ).innerText = `Tài khoản tối đa ${min} - ${max} ký số`),
            (document.getElementById("tbTKNV").style.display = "block"),
            false
        );
    } else {
        return (document.getElementById(idErr).innerText = ``), true;
    }
}

function kiemTraEmail(value) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (isEmail) {
        return (document.getElementById("tbEmail").innerText = ``), true;
    } else {
        return (
            (document.getElementById(
                "tbEmail"
            ).innerText = ` Email phải đúng định dạng`),
            (document.getElementById("tbEmail").style.display = "block"),
            false
        );
    }
}

function kiemTraMatKhau(value) {
    const re =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;
    var isPass = re.test(value);
    if (isPass) {
        return (document.getElementById("tbMatKhau").innerText = ``), true;
    } else {
        return (
            (document.getElementById(
                "tbMatKhau"
            ).innerText = `Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`),
            (document.getElementById("tbMatKhau").style.display = "block"),
            false
        );
    }
}

function kiemTraLuong(value) {
    if (value >= 1000000 && value <= 20000000) {
        return (document.getElementById("tbLuongCB").innerText = ``), true;
    } else {
        return (
            (document.getElementById(
                "tbLuongCB"
            ).innerText = `Lương cơ bản 1.000.000 - 20.000.000`),
            (document.getElementById("tbLuongCB").style.display = "block"),
            false
        );
    }
}
function kiemTraGioLam(value) {
    if (value >= 80 && value <= 200) {
        return (document.getElementById("tbGiolam").innerText = ``), true;
    } else {
        return (
            (document.getElementById(
                "tbGiolam"
            ).innerText = `Số giờ làm trong tháng 80 - 200 giờ`),
            (document.getElementById("tbGiolam").style.display = "block"),
            false
        );
    }
}
